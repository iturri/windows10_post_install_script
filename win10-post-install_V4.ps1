<# 
Auteur : Iturri
E-mail : iturri@tutanota.com
Date: 11/05/2023
Source: https://codeberg.org/iturri/windows10_post_install_script.git

.SYNOPSIS
Post-installation Windows 10.

.DESCRIPTION
Sript post-installation Windows 10 :
-> installation des programmes utiles et désintallation des programmes inutiles 
-> afficher l'extensions des fichiers
-> activater Framework .NET
-> désactivation Internet Explorer
-> désactivation Actualitées Windows
-> désactivation des sons du système
-> désactivation Bing Saerch
-> désactivation Lock screen
-> désactivation de la "Démarrage rapide (fast start-up)"
-> nettoyage Windows


##################################################
Instrution d'exécution du script :
##################################################

1) 
Exécutez le script avec privilèges d'administrateur

2)
Si vous avez l'erreur "l’exécution de scripts est désactivée sur ce système", ouvrez Powershell en tant qu'administrateur 
et executez  la commande : 'Set-ExecutionPolicy -ExecutionPolicy RemoteSigned'.

#>   


function Get-UserPermitions {

    param([switch]$Elevated)

    function Test-Admin {
        $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
        $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
    }

    if ((Test-Admin) -eq $false)  {
        if ($elevated) {
            # tried to elevate, did not work, aborting
        } else {
            Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
        exit
    }
}

function installUninstall {
    # Actualise la Microsoft Store apps
    Get-CimInstance -Namespace "Root\cimv2\mdm\dmmap" -ClassName "MDM_EnterpriseModernAppManagement_AppManagement01" | Invoke-CimMethod -MethodName UpdateScanMethod

    # Installer des programmes utils
    winget install Microsoft.WindowsTerminal
    winget install Microsoft.PowerShell
    winget install vscodium
    winget install Mozilla.Firefox
    winget install TheDocumentFoundation.LibreOffice
    winget install VideoLAN.VLC
    winget install 7zip.7zip
    winget install Adobe.Acrobat.Reader.64-bit
    winget install GIMP.GIMP

    # Désinstaller des programmes inutiles
    winget uninstall Disney.37853FC22B2CE_6rarf9sa4v8jt
    winget uninstall Microsoft.BingNews_8wekyb3d8bbwe
    winget uninstall Microsoft.BingWeather_8wekyb3d8bbwe
    winget uninstall Microsoft.GamingApp_8wekyb3d8bbwe
    winget uninstall Microsoft.MicrosoftOfficeHub_8wekyb3d8bbwe
    winget uninstall Microsoft.MicrosoftSolitaireCollection_8wekyb3d8bbwe
    winget uninstall Microsoft.OneDriveSync_8wekyb3d8bbwe
    winget uninstall Microsoft.People_8wekyb3d8bbwe
    winget uninstall "WildTangent wildgames Master Uninstall"
    winget uninstall Microsoft.WindowsFeedbackHub_8wekyb3d8bbwe
    winget uninstall Microsoft.Xbox.TCUI_8wekyb3d8bbwe
    winget uninstall Microsoft.XboxGameOverlay_8wekyb3d8bbwe
    winget uninstall Microsoft.XboxGamingOverlay_8wekyb3d8bbwe
    winget uninstall Microsoft.XboxIdentityProvider_8wekyb3d8bbwe
    winget uninstall Microsoft.XboxSpeechToTextOverlay_8wekyb3d8bbwe
    winget uninstall Microsoft.ZuneMusic_8wekyb3d8bbwe
    winget uninstall Microsoft.ZuneVideo_8wekyb3d8bbwe
    winget uninstall Microsoft.OneDrive
    winget uninstall SpotifyAB.SpotifyMusic_zpdnekdrzrea0
    # Uninstall Cortana
    winget uninstall Microsoft.549981C3F5F10_8wekyb3d8bbwe

}

<################################################################################################################
Afficher les extensions des fichiers, source :
http://superuser.com/questions/666891/script-to-set-hide-file-extensions
################################################################################################################>
function Get-ExtentionsDisplayed {
    Push-Location
    Set-Location HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
    Set-ItemProperty . HideFileExt "0"
    Pop-Location
    Stop-Process -processName: Explorer -force # Redémarrer le service Explorer 
}


#Activation Framework .NET
function Set-FrameworkDotNet {
    Enable-WindowsOptionalFeature -FeatureName IIS-ASP -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName IIS-ASPNET -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName IIS-ASPNET45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName IIS-NetFxExtensibility -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName IIS-NetFxExtensibility45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName NetFx4-AdvSrvs -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName NetFx4Extended-ASPNET45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-HTTP-Activation -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-HTTP-Activation45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-MSMQ-Activation45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-Pipe-Activation45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName NetFx4Extended-ASPNET45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WAS-NetFxEnvironment -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-TCP-Activation45 -Online -All -NoRestart
    Enable-WindowsOptionalFeature -FeatureName WCF-TCP-PortSharing45 -Online -All -NoRestart
}

<################################################################################################################
Redémarrer la machine
################################################################################################################>
function Start-Demarrage { 
    Write-Host "L'ordinateur va redémarrer en ... `n"

    5..1 | ForEach-Object {"$_"; Start-Sleep -Seconds 1} 

    Restart-Computer
}

Get-UserPermitions
installUninstall
Get-ExtentionsDisplayed
Set-FrameworkDotNet

#Désactiver Internet Explorer
Disable-WindowsOptionalFeature -FeatureName Internet-Explorer-Optional-amd64 -Online -NoRestart

#Désactivation Actualitées Windows
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds" -Name "ShellFeedsTaskbarViewMode" -Value 2 -force

#Désactivation des sons du système
New-ItemProperty -Path HKCU:\AppEvents\Schemes -Name "(Default)" -Value ".None" -Force | Out-Null

#Désactiver Bing Saerch
reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Search" /v BingSearchEnabled /t reg_dword /d 0 /f

#Désactiver Lock screen
reg add "HKEY_CURRENT_USER\SOFTWARE\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsSpotlightFeatures  /t reg_dword /d 1 /f 


#Désactivation de la "Démarrage rapide (fast start-up)"
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Power" /v HiberbootEnabled /t reg_dword /d 0 /f

#nettoyage Windows
Invoke-expression "Dism.exe /online /Cleanup-Image /StartComponentCleanup"
invoke-expression "Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase"

<################################################################################################################
Redéfinition de la politique d'exécution 
################################################################################################################>
Set-ExecutionPolicy -ExecutionPolicy Undefined

Start-Demarrage
